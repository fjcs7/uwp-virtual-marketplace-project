﻿using SCommerceMain.ViewModels;
using Windows.UI.Xaml.Controls;

// O modelo de item de Página em Branco está documentado em https://go.microsoft.com/fwlink/?LinkId=234238

namespace SCommerceMain.Views
{
    /// <summary>
    /// Uma página vazia que pode ser usada isoladamente ou navegada dentro de um Quadro.
    /// </summary>
    public sealed partial class ProductDetailsPage : Page
    {
        private ProductDetailsPageViewModel ViewModel => (ProductDetailsPageViewModel)this.DataContext;
        public ProductDetailsPage()
        {
            this.InitializeComponent();
            //this.DataContext = new ProductDetailsPageViewModel();
        }
    }
}
