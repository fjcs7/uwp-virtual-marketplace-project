﻿using SCommerceMain.ViewModels;
using Windows.UI.Xaml.Controls;


// O modelo de item de Controle de Usuário está documentado em https://go.microsoft.com/fwlink/?LinkId=234236

namespace SCommerceMain.Views
{
    public sealed partial class HeaderView : UserControl
    {
        public HeaderViewModel ViewModel => (HeaderViewModel)DataContext;
        public HeaderView()
        {
            this.InitializeComponent();
        }
    }
}
