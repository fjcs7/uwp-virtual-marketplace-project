﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace SCommerceMain.Controls
{
    public sealed class BadgeAppBarButton : AppBarButton
    {
        private const string STATE_BADGE_HIDDEN = "BadgeHidden";
        private const string STATE_BADGE_VISIBLE = "BadgeVisible";

        public string Badge
        {
            get { return (string)GetValue(BadgeProperty); }
            set { SetValue(BadgeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Badge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BadgeProperty =
            DependencyProperty.Register("Badge", typeof(string), typeof(BadgeAppBarButton), new PropertyMetadata(string.Empty, OnBadgePropertyChange));

        private static void OnBadgePropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var badge = (e.NewValue as string);
            badge = (badge == string.Empty ? badge = "0" : badge);

            var newState = (badge == "0" ? STATE_BADGE_HIDDEN : STATE_BADGE_VISIBLE);
            VisualStateManager.GoToState((Control)d, newState, false);
        }

        public BadgeAppBarButton()
        {
            this.DefaultStyleKey = typeof(BadgeAppBarButton);
        }
    }
}
