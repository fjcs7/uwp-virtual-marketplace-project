﻿using Prism.Events;
using Prism.Mvvm;
using Prism.Windows.Navigation;
using SCommerceMain.Common;
using SCommerceMain.Events;

namespace SCommerceMain.ViewModels
{
    public class HeaderViewModel : BindableBase
    {
        private readonly INavigationService navigationService;
        private readonly IEventAggregator eventAgreggator;

        private int cartQuantity;
        public int CartQuantity
        {
            get { return cartQuantity; }
            set { SetProperty(ref cartQuantity, value); }
        }

        public HeaderViewModel(INavigationService navigationService, IEventAggregator eventAgreggator)
        {
            this.navigationService = navigationService;
            this.eventAgreggator = eventAgreggator;

            eventAgreggator.GetEvent<AddedToCardEvent>().Subscribe(HandleAddedToCardEvent);
        }

        private void HandleAddedToCardEvent(AddedToCardEvent.Payload payload)
        {
            CartQuantity += payload.Quantity;
        }

        public void NavigateToCartPage()
        {
            navigationService.Navigate(PageTokens.CartPage, null);
        }
    }
}
