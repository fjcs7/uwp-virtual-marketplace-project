﻿using System;
using Windows.UI.Xaml.Data;

namespace SCommerceMain.Converters
{
    public class DoubleToCurrencyFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is double num)
            {
                return num.ToString("C2");
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
