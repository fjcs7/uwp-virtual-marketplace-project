﻿using Prism.Unity.Windows;
using SCommerceMain.Common;
using SCommerceMain.Services;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace SCommerceMain
{
    /// <resumo>
    ///Fornece o comportamento específico do aplicativo para complementar a classe Application padrão.
    /// </summary>
    public sealed partial class App : PrismUnityApplication
    {
        protected override Task OnLaunchApplicationAsync(LaunchActivatedEventArgs args)
        {
            NavigationService.Navigate(PageTokens.ProductDetailsPage, null);
            return Task.CompletedTask;
        }

        protected override UIElement CreateShell(Frame rootFrame)
        {
            var appShell = new AppShell();
            appShell.SetFrame(rootFrame);
            return appShell;
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            RegisterTypeIfMissing(typeof(IProductService), typeof(ProductService), false);
            RegisterTypeIfMissing(typeof(ICartService), typeof(CartService), true);
        }
    }
}
