﻿using Prism.Events;

namespace SCommerceMain.Events
{
    public class AddedToCardEvent : PubSubEvent<AddedToCardEvent.Payload>
    {
        public class Payload
        {
            public int ProductId { get; set; }
            public int Quantity { get; set; }
        }
    }
}
