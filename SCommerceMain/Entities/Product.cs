﻿using System.Collections.Generic;

namespace SCommerceMain.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public double Price { get; set; }
        public int Rating { get; set; }
        public List<string> Images { get; set; }
    }
}
