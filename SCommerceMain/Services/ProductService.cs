﻿using SCommerceMain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SCommerceMain.Services
{
    public class ProductService : IProductService
    {
        public Task<Product> FindAsync(int id)
        {
            var result = new Product
            {
                Id = 1,
                Title = "Product from Service",
                Description = "A medium discription for this product, for test only...",
                Price = 120.62596,
                Rating = 4,
                Images = new List<string>
                            {
                                "ms-appx:///Assets/Images/T-shirts/universe.jpg",
                                "ms-appx:///Assets/Images/T-shirts/the_queen.jfif",
                                "ms-appx:///Assets/Images/T-shirts/coffee.jfif"
                            }
            };
            return Task.FromResult(result);
        }
    }
}
