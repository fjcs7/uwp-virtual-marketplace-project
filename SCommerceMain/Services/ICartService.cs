﻿namespace SCommerceMain.Services
{
    public interface ICartService
    {
        void Add(int productId, int quantity);
    }
}