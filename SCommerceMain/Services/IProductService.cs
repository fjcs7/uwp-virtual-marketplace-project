﻿using SCommerceMain.Entities;
using System.Threading.Tasks;

namespace SCommerceMain.Services
{
    public interface IProductService
    {
        Task<Product> FindAsync(int id);
    }
}