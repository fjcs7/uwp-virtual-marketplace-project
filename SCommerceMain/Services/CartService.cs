﻿using Prism.Events;
using SCommerceMain.Events;
using System.Collections.Generic;

namespace SCommerceMain.Services
{
    public class CartService : ICartService
    {
        private readonly IEventAggregator eventAggregator;
        private Dictionary<int, int> cart;
        public CartService(IEventAggregator eventAggregator)
        {
            cart = new Dictionary<int, int>();
            this.eventAggregator = eventAggregator;
        }
        public void Add(int productId, int quantity)
        {
            if (cart.ContainsKey(productId))
            {
                cart[productId] += quantity;
            }
            else
            {
                cart.Add(productId, quantity);
            }

            eventAggregator.GetEvent<AddedToCardEvent>().Publish(new AddedToCardEvent.Payload
            {
                ProductId = productId,
                Quantity = quantity
            });
        }
    }
}
