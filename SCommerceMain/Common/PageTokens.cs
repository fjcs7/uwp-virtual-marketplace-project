﻿namespace SCommerceMain.Common
{
    public static class PageTokens
    {
        public const string ProductDetailsPage = "ProductDetails";
        public const string CartPage = "Cart";
    }
}
